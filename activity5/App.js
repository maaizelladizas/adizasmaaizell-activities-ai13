import React from 'react';
import { Text, View,TouchableHighlight,} from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state= {
      primaryDisplay: '0',
      clear: true,
    }

    this.changeMod = this.changeMod.bind(this);
    this.clearScreen = this.clearScreen.bind(this);
  }

  
    operationButtons(value, functionCall = null) {
      return(
        <View style= {styles.calculator}>
          <TouchableHighlight
            style={styles.primaryButtons}
            onPress={() =>  functionCall ? functionCall() : this.onClickHandler(value)}
          >
            <Text style={styles.primaryButtonsText}> {value} </Text>
          </TouchableHighlight>
        </View>
      );
    }
  
    onClickHandler(value) {
      const { primaryDisplay, clear } = this.state;
      let newValue = clear ? `${value}` : primaryDisplay.concat('' + value);
      this.setState({
        primaryDisplay: newValue,
        clear: false,
      })
    }
  
    getResult() {
      const { primaryDisplay } = this.state;
      try {
        let submitString = primaryDisplay.replace('x', '*').replace('÷', '/');
        let result = `${eval(submitString)}`;
       
        this.setState({
          primaryDisplay: result,
        });
      } catch(e) {
          console.log(e);
      }
    }
  
    clearScreen() {
      this.setState({
        primaryDisplay: '0',
        clear: true,
      })
    }
  
    backPress() {
      const { clear, primaryDisplay } = this.state;
      let displayArray =  primaryDisplay.split(' ');
  
      if (displayArray.length === 1) {
        const result = clear
        ? `${primaryDisplay}`
        : primaryDisplay.substr(0, primaryDisplay.length - 1);
        this.setState({
          primaryDisplay: result.length === 0 ? '0' : result,
          clear: result.length === 0,
        })
      } else {
        displayArray.pop();
        const result = displayArray.join(' ');
        this.setState({
          primaryDisplay: result,
        })
      }
    }
  
    digitsButtons(value) {
      return(
        <View style= {styles.calculator}>
          <TouchableHighlight
            style={styles.twoButtons}
            onPress={() => this.onClickHandler(value)}
          >
            <Text style={styles.twoButtonsText}>{value}</Text>
          </TouchableHighlight>
        </View>
      )
    }
  
    changeMod() {
      const { primaryDisplay } = this.state;
      const negMod = '-(';
      let res = '';
      if (primaryDisplay.includes(negMod)) {
        res = primaryDisplay.replace('-(', '').replace(')', '');
      } else {
        res = primaryDisplay === '0' ? primaryDisplay : `-(${primaryDisplay})`;
      }
  
      this.setState({
        primaryDisplay: res,
      })
    }

    equalsButton() {
      return(
        <View style= {styles.calculatorDouble}>
          <TouchableHighlight
            style={[styles.primaryButtons, {backgroundColor: '#F27030'}]}
            onPress={() => this.getResult()}
          >
            <Text style={styles.primaryButtonsText}> = </Text>
          </TouchableHighlight>
        </View>
      );
    }
  
    row1() {
      return(
        <View style={styles.rowLayout}>
          {this.operationButtons('C', this.clearScreen)}
          {this.operationButtons('+/-', this.changeMod)}
          {this.operationButtons('%')}
          {this.operationButtons('÷')}
        </View>
  
      );
    }
  
    row2() {
      return(
        <View style={styles.rowLayout}>
          {this.digitsButtons(7)}
          {this.digitsButtons(8)}
          {this.digitsButtons(9)}
          {this.operationButtons('x')}
        </View>
      );
    }
  
  
    row3() {
      return(
        <View style={styles.rowLayout}>
          {this.digitsButtons(4)}
          {this.digitsButtons(5)}
          {this.digitsButtons(6)}
          {this.operationButtons('-')}
        </View>
      );
    }
  
    row4() {
      return(
        <View style={styles.rowLayout}>
          {this.digitsButtons(1)}
          {this.digitsButtons(2)}
          {this.digitsButtons(3)}
          {this.operationButtons('+')}
      </View>
      );
    }
  
    row5() {
      return(
        <View style={styles.rowLayout}>
          {this.digitsButtons(0)}
          {this.digitsButtons('.')}
          {this.equalsButton()}
      </View>
      );
    }

    render() {
      const { primaryDisplay } = this.state;
      return(
        <View  style={styles.appContainer}>
          <View style={styles.head}>
            <View style={styles.headContainer}>
              <View style={{ width: '100%', flexDirection: 'row-reverse', marginTop: 30 }}>
                <Text style={{ fontSize: 20, color: '#848A8F' }}> </Text>
              </View>
              <View style={{ flexDirection: 'row-reverse' , width: '100%', marginTop: 25}}>
                <Text style={{ fontSize: 28, color: 'white' }}> {primaryDisplay} </Text>
              </View>
  
              <View style={{ flexDirection: 'row' }}>
                <View style={{ width: '50%'}}>
                  <TouchableHighlight
                    onPress={() => this.toggleModal()}
                  >
                    <View style={{ width: 0, height: 0 }}></View>
                  </TouchableHighlight>
                </View>
                <View style={{ width: '50%', flexDirection: 'row-reverse' }}>
                  <TouchableHighlight
                    onPress={() => this.backPress()}
                  >
                    <View style={{ width: 30, height: 18 }}></View>
                  </TouchableHighlight>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.body}>
            {this.row1()}
            {this.row2()}
            {this.row3()}
            {this.row4()}
            {this.row5()}
          </View>
  
        </View>
      );
  }
}


const styles = {
  appContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#06121B',
  },
  head: {
    height: '26%',
    marginTop: 25,
  },

  headContainer: {
    width: '95%',
    height: '100%',
    alignSelf: 'center',
  },
  body: {
    width: '100%',
    height: '71%',
  },
  rowLayout: {
    flexDirection: 'row',
    marginTop: 3,
  },
  calculator: {
    width: `${100/4}%`,
    height: 90,
  },
  calculatorDouble: {
    width: `${100/2}%`,
    height: 90,
  },
  primaryButtons: {
    width: '98%',
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2ce82c',
    borderRadius:50
  },
  
  oneButtonsText: {
    color: 'black',
    fontSize: 28,
    fontWeight: 'bold',
  },
  twoButtons :{
    width: '98%',
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3F4F5B'
  },
  twoButtonsText: {
    fontSize: 28,
    color: 'white',
  },
};