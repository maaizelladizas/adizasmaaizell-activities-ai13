import { Text, View } from 'react-native';

export default function CompleteScreen() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Completed Tasks Go Here</Text>
      </View>
    );
  }
  