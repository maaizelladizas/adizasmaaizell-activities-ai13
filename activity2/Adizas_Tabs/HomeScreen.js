import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Keyboard, ScrollView } from 'react-native';
import Task from '../aiz/elle';

export default function HomeScreen() {
    const [task, setTask] = useState();
    const [taskItems, setTaskItems] = useState([]);
    const handleAddTask = () => {
        Keyboard.dismiss();
        setTaskItems([...taskItems, task])
        setTask("");
    }
    const completeTask = (index) => {
        let itemsCopy = [...taskItems];
        itemsCopy.splice(index, 1);
        setTaskItems(itemsCopy)
    }

    return (
        <View style={styles.write_taskcomponent}>
            <TextInput style={styles.text_component} placeholder={'Write a task'} value={task} onChangeText={text => setTask(text)} />
            <View style={styles.btn_taskcomponent}>
                <TouchableOpacity onPress={() => handleAddTask()}>
                    <Text style={styles.add_taskcomponent}>Add Task</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.added_taskcomponent} contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='handled'>
                <View style={styles.items}>
                    {
                        taskItems.map((item, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => completeTask(index)}>
                                    <Task text={item} />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>
    );
}
    
const styles = StyleSheet.create({
    write_taskcomponent: {
        marginTop: 60,
        flex: 1,
        position: 'relative',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
},

    added_taskcomponent: {
        marginTop: 10,
        width: '90%',
    },
    text_component: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: '#FFF',
        borderColor: '#000',
        borderWidth: 2,
        borderRadius: 6,
        width: '90%',
    },
    btn_taskcomponent: {
        marginTop: 20,
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#30d148',
        borderColor: '#000',
        borderWidth: 2,
        borderRadius: 10,
        width: '90%',
    },
    add_taskcomponent: {
        paddingHorizontal: 15,
        backgroundColor: '#30d148',
        textAlign: 'center',
        color: '#fff',
        fontWeight: 'bold',
    },
});
  