import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from './Adizas_Tabs/HomeScreen';
import SettingsScreen from './Adizas_Tabs/SettingsScreen';
import CompleteScreen from './Adizas_Tabs/Completed';

export default function App() {
    return (
        <NavigationContainer>
      <Tab.Navigator
              screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                  let iconName;

                  if (route.name === 'Home') {
                    iconName = 'home';
                  } else if (route.name === 'Settings') {
                    iconName = 'settings';
                  } else if (route.name === 'Complete') {
                    iconName = 'filter';
                    
                  }
                  // You can return any component that you like here!
                  return <Ionicons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: 'green',
                tabBarInactiveTintColor: 'gray',
              })}
      >
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Settings" component={SettingsScreen} />
        <Tab.Screen name="Complete" component={CompleteScreen} />
      </Tab.Navigator>
    </NavigationContainer>
    );
}
const Tab = createBottomTabNavigator();