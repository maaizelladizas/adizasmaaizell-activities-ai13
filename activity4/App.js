import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screens/Home'
import FirstScreen from './screens/FirstScreen';
import SecondScreen from './screens/SecondScreen';
import ThirdScreen from './screens/ThirdScreen';
import FourthScreen from './screens/FourthScreen';
import FifthScreen from './screens/FifthScreen';

export default function App() {
  return (
    <NavigationContainer >
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="FirstScreen" component={FirstScreen} options={{headerShown: false}} />
        <Stack.Screen name="SecondScreen" component={SecondScreen} options={{headerShown: false}} />
        <Stack.Screen name="ThirdScreen" component={ThirdScreen} options={{headerShown: false}} />
        <Stack.Screen name="FourthScreen" component={FourthScreen} options={{headerShown: false}} />
        <Stack.Screen name="FifthScreen" component={FifthScreen} options={{headerShown: false}} />
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );  
}
const Stack = createNativeStackNavigator();