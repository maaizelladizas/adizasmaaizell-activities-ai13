import * as React from 'react';
import { StyleSheet ,View , Text , Button , Image , TouchableOpacity} from 'react-native';

const SecondScreen = ({ navigation }) => {
    return (
      <View style={styles.lists}>
        <Image style={styles.image} source={require('../ITImg/designergirl.png')}></Image>
        <View style={styles.textCon}>
          <Text style={styles.text}>
            Interested to editing.
          </Text>
        </View>
        <View style={{flex: 1,justifyContent: 'flex-end'}}>
          <TouchableOpacity style={styles.buttonCon}
            onPress={() =>
            navigation.navigate('ThirdScreen')}>
            <Text style={styles.textBut}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
    
  const styles = StyleSheet.create({
    lists: {
        justifyContent: 'center',
        flex: 1,
        padding:10,
        paddingTop: 80,
      
    },
    textCon:{
        borderWidth: 2,
        marginTop: 30,
        padding:10,
        paddingLeft: 55,
        borderRadius: 50,
        borderColor: "black",
        width: '100%', 
    },
    text: {
      color:'black',
      fontSize: 30
    },
    textBut: {
        color:'black',
        fontSize: 20,
        fontWeight: "bold",
    },
    buttonCon:  {
      alignItems: "center",
      height: 50,
      borderRadius: 50,
      paddingVertical: 10,
      backgroundColor: '#02f71f',
 
    },
    image: {
        height:350,
        borderWidth: 2, 
        borderRadius: 20,
        borderColor: "#02f71f",
        width: '100%', 

    }
  });
export default SecondScreen;