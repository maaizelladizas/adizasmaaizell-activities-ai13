import * as React from 'react';
import {StyleSheet, Text, SafeAreaView, ScrollView, StatusBar , Image, View} from 'react-native';

export default HomeScreen = () => {
    return (
        <View style={styles.lists}>
            <View style={styles.txt_container}>
                <Text style={styles.welcome}>Welcome to home.</Text>
            </View>
        </View>
    );
  };
  
  const styles = StyleSheet.create({
    lists: {
      flex: 1,
      padding:10,
      paddingTop: 80,
      alignItems: "center",
      justifyContent: 'center',
    },
    welcome:{
        fontWeight: "bold",
        fontSize: 30,
        color: '#02f71f',
    },
    txt_container:{
        borderWidth: 2,
        marginTop: 30,
        padding:10,
        paddingLeft: 55,
        borderRadius: 50,
        borderColor: "black",
        width: '100%', 
    },
});